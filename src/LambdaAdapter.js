const Configurapi = require('configurapi');
const URL = require('url');

module.exports = {
    toResponse: function(response)
    {
        return {
            statusCode: response.statusCode,
            headers: response.headers,
            body: JSON.stringify(response.body)
        };
    },

    toRequest: function(awsRequest)
    {
        let request = new Configurapi.Request();

        request.name = process.env.event_name || ''
        request.method = (awsRequest.httpMethod || '').toLowerCase();
        request.headers = {};
        
        for(let headerKey of Object.keys(awsRequest.headers || {}))
        {
            request.headers[headerKey.toLowerCase()] = awsRequest.headers[headerKey];
        }
 
        request.payload = process.env.event_payload || awsRequest.body;
        
        request.query = {};
        for(let queryKey of Object.keys(awsRequest.queryStringParameters || {}))
        {
            request.query[queryKey.toLowerCase()] = awsRequest.queryStringParameters[queryKey];
        }

        request.path = awsRequest.path || '';
        request.pathAndQuery = awsRequest.path || '';

        try
        {
            if(request.payload)
            {
                request.payload = JSON.parse(request.payload);  
            }
        }
        catch(e){} 

        return request;
    }
};
